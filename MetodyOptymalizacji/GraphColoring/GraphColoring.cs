using System;
using System.Collections.Generic;
using GraphLibrary;

namespace GraphColoring
{
    public static class GraphColoring
    {
        public static void FindSolution(Graph graph)
        {
            int colors = 0;
            List<Node> unColoredNodes = new List<Node>(graph.GetAllNodes());

            while (unColoredNodes.Count != 0)
            {
                colors += 1;
                List<Node> independentSet = FindMaximalIndependentSet(unColoredNodes);
                SetColorForSet(independentSet, graph);
                RemoveIndependentSetFromUnColoredNodes(unColoredNodes, independentSet);
            }

            PrintResult(graph, colors);
        }

        private static void PrintResult(Graph graph, int colors)
        {
            Console.WriteLine($"Do pokolorowania grafu użyto {colors} kolorów.");
            int counter = 0;
            foreach (var groupSet in graph.GroupsSets)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Kolor {counter}:");
                Console.ForegroundColor = ConsoleColor.Black;

                foreach (var node in groupSet.NodesInGroup)
                {
                    Console.Write($"{node.Name}; ");
                }

                Console.WriteLine();
                counter++;
            }
        }

        private static void RemoveIndependentSetFromUnColoredNodes(List<Node> unColoredNodes, List<Node> independentSet)
        {
            foreach (var node in independentSet)
            {
                unColoredNodes.Remove(node);
            }
        }

        private static void SetColorForSet(List<Node> independentSet, Graph graph)
        {
            List<String> names = new List<String>();
            foreach (var node in independentSet)
            {
                names.Add(node.Name);
            }
            graph.AddGroup(names);
        }

        private static List<Node> FindMaximalIndependentSet(List<Node> unColoredNodes)
        {
            List<Node> localUnColoredNodes = new List<Node>(unColoredNodes);
            List<Node> independentSet = new List<Node>();

            localUnColoredNodes.Sort((node, node1) => node.CountOfNeighbors.CompareTo(node1.CountOfNeighbors));
            while (localUnColoredNodes.Count != 0)
            {
                var node = localUnColoredNodes[0];
                var neighbors = new List<Node>();
                node.Neighbors.ForEach(tuple => neighbors.Add(tuple.node));
                
                independentSet.Add(node);
                localUnColoredNodes.Remove(node);
                for (int i = 0; i < localUnColoredNodes.Count; i++)
                {
                    if (neighbors.Contains(localUnColoredNodes[i]))
                    {
                        localUnColoredNodes.RemoveAt(i);
                    }
                }
            }

            return independentSet;
        }
    }
}