﻿using GraphLibrary;

namespace GraphColoring
{
    class Program
    {
        static void Main(string[] args)
        {
            var graphs = GraphLoader.LoadFromFile("Data//GraphColoring.txt");
            foreach (var graph in graphs)
            {
                GraphColoring.FindSolution(graph);
            }
        }
    }
}