﻿using System;
using GraphLibrary;

namespace EulerCycle
{
    class Program
    {
        static void Main(string[] args)
        {
            var graphs = GraphLoader.LoadFromFile("Data//EulerCycle.txt");
            EulerCycle eulerCycle = new EulerCycle();

            int counter = 1;
            var defaultConsole = Console.ForegroundColor;
            foreach (var graph in graphs)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Graf {counter}:");
                Console.ForegroundColor = defaultConsole;
                eulerCycle.GetEulerCycle(graph);
                counter++;
            }
        }
    }
}