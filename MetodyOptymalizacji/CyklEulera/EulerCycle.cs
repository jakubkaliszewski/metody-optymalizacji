using System;
using System.Collections.Generic;
using System.Linq;
using GraphLibrary;

namespace EulerCycle
{
    public class EulerCycle
    {
        private bool IsEulerCycleExist(Graph graph)
        {
            if (graph.IsConnectedGraph)
            {
                if (graph.IsDirectedGraph)
                {
                    foreach (var node in graph.GetAllNodes())
                    {
                        int outputEdges = node.CountOfNeighbors;
                        int inputEdges = 0;
                        foreach (var inputNode in graph.GetAllNodes())
                        {
                            if (inputNode.Neighbors.Exists(tuple => tuple.node == node))
                                inputEdges++;
                        }

                        if (outputEdges != inputEdges)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    foreach (var node in graph.GetAllNodes())
                    {
                        if (node.CountOfNeighbors % 2 != 0)
                            return false;
                    }
                }
            }

            return true;
        }

        public void GetEulerCycle(Graph graph)
        {
            if (!IsEulerCycleExist(graph))
            {
                Console.WriteLine("Cykl Eulera w grafie nie istnieje!!");
                return;
            }

            FindEulerCycle(graph);
        }

        private void FindEulerCycle(Graph graph)
        {
            var resultList = new List<Node>();
            Stack<Node> stack = new Stack<Node>();
            var nodes = new List<Node>(graph.GetAllNodes());
            stack.Push(nodes[0]);

            while (stack.Any())
            {
                var actualNode = stack.Peek();//pobiera obiekt ze stosu bez zdejmowania go
                if (actualNode.CountOfNeighbors == 0)
                {
                    stack.Pop();
                    resultList.Add(actualNode);
                }
                else
                {
                    var neighbor = actualNode.Neighbors[0].node;
                    stack.Push(neighbor);
                    graph.RemoveEdgeByName(actualNode.Name, neighbor.Name);
                }
                
            }

            resultList.Reverse();
            foreach (var node in resultList)
            {
                Console.Write($"{node.Name} => ");
            }
            Console.WriteLine("Koniec");    
        }
    }
}