﻿using System;
using GraphLibrary;

namespace HungarianAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            var graphs = GraphLoader.LoadFromFile("Data//HungarianAlgorithm.txt");
            foreach (var graph in graphs)
            {
                HungarianAlgorithm.FoundSolution(graph);
            }
        }
    }
}