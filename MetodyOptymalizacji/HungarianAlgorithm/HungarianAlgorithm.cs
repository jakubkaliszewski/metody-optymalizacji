using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphLibrary;
using static MaximalMatching.MaximalMatching;

namespace HungarianAlgorithm
{
    public static class HungarianAlgorithm
    {
        private static List<Node> setS;
        private static List<Node> setY;
        private static List<Node> setT;

        public static void FoundSolution(Graph graph)
        {
            if (graph.GroupsSets[0].NodesInGroup.Count != graph.GroupsSets[1].NodesInGroup.Count)
                throw new Exception(
                    "Wprowadzony graf musi być 2-dzielny i posiadać grupy o identycznej liczbie wierzchołków!");
            SetInitialGraphLabeling(graph);
            SetDirectedEdges(graph);

            var firstGroup = graph.GroupsSets[0]; //Grupa V1
            
            while (!graph.GraphHasPerfectMatching())
            {
                Node freeNode = firstGroup.NodesInGroup.Find(node => node.ConnectedNode == null);
                //czyszczenie S,T, sąsiedziS
                //dodanie freenode do S
                
                //wyznaczenie sąsiadów zbioru S
                //sprawdzenie czy T = sąsiadom całego zbioru S
                //jeśli równe to zmiana etykiet, zbudowanie nowego grafu równego
                //czyszczenie sąsiadów S, 
                Node[] augmentingPath = null;
                while (augmentingPath == null)
                {
                    augmentingPath = FindAughmentingPath(freeNode);
                    if (augmentingPath == null)
                    {
                        ImproveLabeling(graph);
                    }
                }
                ChangeDirectionsInTheGraph(augmentingPath);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Zakończono!");
            Console.ForegroundColor = ConsoleColor.Black;
            //jeżeli skojarzenie pełne to stop
        }
       
        private static void SetInitialMatching(Node node, Graph graph)
        {
            var nodeToMatching = node.Neighbors.Find(tuple => tuple.wage == node.Label);
            graph.AddConnectionByName(node.Name, nodeToMatching.node.Name);
            graph.IsMatchingGraph = true;
        }

        private static void SetInitialGraphLabeling(Graph graph)
        {
            var firstGroup = graph.GroupsSets[0].NodesInGroup;
            foreach (var node in firstGroup)
                node.Label = 0;

            var secondGroup = graph.GroupsSets[1].NodesInGroup;
            secondGroup.ForEach(node =>
            {
                node.Label = GetMaxEdgeWageForNode(node, graph);
                SetInitialMatching(node, graph);
            });
        }

        private static int GetMaxEdgeWageForNode(Node node, Graph graph)
        {
            int actualMax = 0;
            var neighbors = node.Neighbors;

            foreach (var neighbor in neighbors)
            {
                if (neighbor.wage > actualMax)
                    actualMax = neighbor.wage;
            }

            return actualMax;
        }

        private static void ImproveLabeling(Graph graph)
        {
            setS = VisitedNodes.FindAll(node => node.GroupID == 0);
            setT = VisitedNodes.FindAll(node => node.GroupID == 1);
            setY = graph.GetAllNodes().FindAll(node => !VisitedNodes.Contains(node) && node.GroupID == 1);
            
            int lambda = CalculateLambda();
            
            graph.GetAllNodes().ForEach(node =>
            {
                if (setS.Contains(node))
                    node.Label -= lambda;
                if (setT.Contains(node))
                    node.Label += lambda;
            });
        }

        private static int CalculateLambda()
        {
            int minimumLambda = Int32.MaxValue;
            foreach (var nodeS in setS)
            {
                foreach (var nodeY in setY)
                {
                    int wage = 0;
                    if (nodeS.IsNeighbor(nodeY))
                    {
                        wage = nodeS.Neighbors.Find(tuple => tuple.node == nodeY).wage;
                        
                    }

                    if (nodeY.IsNeighbor(nodeS))
                    {
                        wage = nodeY.Neighbors.Find(tuple => tuple.node == nodeS).wage;
                    }

                    if (wage != 0)
                    {
                        var actualLambda = nodeS.Label + nodeY.Label - wage;
                        if (actualLambda < minimumLambda)
                            minimumLambda = actualLambda;
                    }
                }
            }

            return minimumLambda;
        }
    }
}