using System;
using System.Collections.Generic;
using System.Linq;
using GraphLibrary;

namespace TravellingSalesmanMST
{
    public static class TravellingSalesmanMST
    {
        private static int sum;
        public static void FindSolution(Graph graph)
        {
            Node selectedNode;
            try
            {
                selectedNode = SelectOneNode(graph);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Nie wprowadzono prawidłowej nazwy wierzchołka!! Błąd: {e}");
                return;
            }

            if (selectedNode != null)
            {
                var minimumSpanningTree = KruskalsAlgorithm(graph);
                IList<Node> hamiltonCycle = GetHamiltonCycleFromMST(minimumSpanningTree, selectedNode);
                PrintSolution(hamiltonCycle);
            }
        }

        private static Graph KruskalsAlgorithm(Graph graph)
        {
            Graph spannigTree = new Graph();
            var sortedEdges = GetEdgesGroupedByWage(graph);
            int counter = 0;
            
            while(counter <= graph.CountOfNodes)
            {
                var firstEdge = sortedEdges[0];
                sortedEdges.RemoveAt(0);
                
                AddMinimalEdgeToSpanningTree(spannigTree,firstEdge);
                if (HasCycle(spannigTree))
                {
                    RemoveEdgeFromTree(spannigTree, firstEdge);
                }
                else
                {
                    counter++;
                }
            }

            return spannigTree;
        }

        private static bool HasCycle(Graph tree)
        {
            List<Node> visited = new List<Node>();
            Stack<Node> stack = new Stack<Node>();//stos wskazuje poprzednika na ścieżce
            var nodesInTree = tree.GetAllNodes();

            foreach (var actualNode in nodesInTree)//na wypadek braku spójności
            {
                if (!visited.Contains(actualNode))
                {
                    if(VisitNode(actualNode, visited, stack))
                        return true;
                }
            }

            if (stack.Count != 0)
                return true;

            return false;
        }

        private static bool VisitNode(Node actualNode, List<Node> visited, Stack<Node> stack)
        {
            if (visited.Contains(actualNode))
                return true;
            
            visited.Add(actualNode);
            Node previousNode = null;
            if (stack.Count != 0)
            {
                previousNode = stack.Peek();
            }
            
            foreach (var neighbor in actualNode.Neighbors)
            {
                if (neighbor.node != previousNode && !visited.Contains(neighbor.node))
                {
                    stack.Push(actualNode);
                    VisitNode(neighbor.node, visited, stack);
                    
                    //przechodzimy dalej
                }                
            }

            stack.TryPop(out var node);
            return false;
        }

        private static void AddMinimalEdgeToSpanningTree(Graph tree, Edge edge)
        {
            tree.AddEdgeByName(edge.Node1.Name, edge.Node2.Name, wage: edge.Wage);
        }

        private static void RemoveEdgeFromTree(Graph tree, Edge edge)
        {
            tree.RemoveEdgeByName(edge.Node1.Name, edge.Node2.Name);
        }
        
        private static IList<Edge> GetEdgesGroupedByWage(Graph graph)
        {
            var nodesInGraph = graph.GetAllNodes();
            List<Edge> edges = new List<Edge>();
            
            foreach (var node in nodesInGraph)
            {
                node.Neighbors.ForEach(tuple =>
                {
                    var added = edges.Exists(edge => (edge.Node1 == node && edge.Node2 == tuple.node) ||
                                         (edge.Node1 == tuple.node && edge.Node2 == node));

                    if (!added)
                        edges.Add(new Edge(node, tuple.node, tuple.wage));
                });
            }

            edges.Sort((edge, edge1) => edge.Wage.CompareTo(edge1.Wage));
            return edges;
        }

        private static IList<Node> GetHamiltonCycleFromMST(Graph tree, Node startNode)
        {
            List<Node> visitedNodes = new List<Node>();
            VisitNode(startNode, visitedNodes);
            visitedNodes.Add(startNode);

            return visitedNodes;
        }

        private static void VisitNode(Node node, IList<Node> visitedNodes)
        {
            visitedNodes.Add(node);
            foreach (var neighbor in node.Neighbors)
            {
                if (!visitedNodes.Contains(neighbor.node))
                    VisitNode(neighbor.node, visitedNodes);
            }
        }

        private static void PrintAllNodes(Graph graph)
        {
            Console.WriteLine("Wierzchołki budujące graf: ");
            graph.GetAllNodes().ForEach(node =>Console.WriteLine($"- {node.HumanName}"));
        }

        private static Node SelectOneNode(Graph graph)
        {
            PrintAllNodes(graph);
            Node selectedNode = null;
            while (selectedNode == null)
            {
                Console.WriteLine("Który wierzchołek grafu wybierasz jako startowy? Proszę wprowadzić widoczną nazwę. \nWybór: ");//wykonać na do while
                var selected = Console.ReadLine().ToUpper();
                selectedNode = graph.GetAllNodes().Find(node => node.HumanName == selected);
            }
            
            return selectedNode;
        }

        private static void PrintSolution(IList<Node> hamiltonCycle)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Rozwiązanie problemu komiwojażera: ");
            Console.ForegroundColor = ConsoleColor.Black;

            foreach (var node in hamiltonCycle)
            {
                Console.Write($"{node.HumanName} => ");
            }
            Console.WriteLine("Koniec");  
        }
    }
}