using GraphLibrary;

namespace TravellingSalesmanMST
{
    class Edge
    {
        public Edge(Node node1, Node node2, int wage)
        {
            Node1 = node1;
            Node2 = node2;
            Wage = wage;
        }
        public Node Node1 { get; set; }
        public Node Node2 { get; set; }
        public int Wage { get; set; }
    }
}