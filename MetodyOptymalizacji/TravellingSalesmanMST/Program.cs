﻿using GraphLibrary;

namespace TravellingSalesmanMST
{
    class Program
    {
        static void Main(string[] args)
        {
            var graphs = GraphLoader.LoadFromFile("Data//TravellingSalesman.txt");
            foreach (var graph in graphs)
            {
                TravellingSalesmanMST.FindSolution(graph);
            }
        }
    }
}