﻿using GraphLibrary;

namespace ZnajdowanieMaksymalnegoSkojarzenia
{
    class Program
    {
        static void Main()
        {
            var graphs = GraphLoader.LoadFromFile("Data//ExtensionPath.txt");
            foreach (var graph in graphs)
            {
                MaximalMatching.MaximalMatching.FoundMaximalMatching(graph);
            }
        }
    }
}