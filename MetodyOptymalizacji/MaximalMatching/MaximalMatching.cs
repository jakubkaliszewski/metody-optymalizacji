using System;
using System.Collections.Generic;
using GraphLibrary;

namespace MaximalMatching
{
    public static class MaximalMatching
    {
        public static List<Node> VisitedNodes { get; set; }
        
        public static void FoundMaximalMatching(Graph graph)
        {
            if (!graph.IsBipartitedGraph)
                throw new Exception("Wprowadzony graf musi być 2-dzielny!");
            if (!graph.IsMatchingGraph)
                CreateInitialMatching(graph);
            SetDirectedEdges(graph);
            
             var group = graph.GroupsSets[0];//Grupa V1
             foreach (var nodeFromGroup in group.NodesInGroup)
             {
                //jeśli nieskojarzona "panna" to łączymy ją z 1. wolnym kawalerem.
                var augmentingPath = FindAughmentingPath(nodeFromGroup);
                ChangeDirectionsInTheGraph(augmentingPath);
             }
             
             PrintResult(graph);
        }
        private static void CreateInitialMatching(Graph graph)
        {
            graph.NonMatchedNodes = graph.CountOfNodes;
            graph.IsMatchingGraph = true;
            
            foreach (var node in graph.GetAllNodes())
            {
                if (node.ConnectedNode == null)
                {
                    var actualNodeGroup = node.GroupID;
                    foreach (var neighbor in node.Neighbors)
                    {
                        if (neighbor.node.GroupID != actualNodeGroup && neighbor.node.ConnectedNode == null)
                        {
                            node.ConnectedNode = neighbor.node;
                            neighbor.node.ConnectedNode = node;
                            graph.NonMatchedNodes -= 2;
                            break;
                        }
                    }
                }
            }
        }

        public static void SetDirectedEdges(Graph graph)
        {
            var nodes = graph.GetAllNodes();
            int countOfNodes = nodes.Count;
            
            for (int i = 0; i < countOfNodes; i++)
            {
                var node = nodes[i];
                
                if (node.ConnectedNode != null)
                {
                    if(node.GroupID == 1)
                        graph.ChangeEdgeToDirected(node, node.ConnectedNode);
                }
                else if (node.GroupID == 0)
                {
                    var nodeNeighbors = node.Neighbors;
                    int countOfNodeNeighbors = nodeNeighbors.Count;

                    for (int j = 0; j < countOfNodeNeighbors; j++)
                    {
                        var nodeNeighbor = nodeNeighbors[j];
                        graph.ChangeEdgeToDirected(node,nodeNeighbor.node);  
                    }
                }
            }
        }

        public static Node[] FindAughmentingPath(Node startNode)
        {
            if (startNode.ConnectedNode != null)
                return null;
            
            VisitedNodes = new List<Node>();
            Stack<Node> path = new Stack<Node>();
            Node actualNode;
            
            VisitedNodes.Add(startNode);
            path.Push(startNode);
            
            for (int i = 0; i < startNode.Neighbors.Count; i++)
            {
                actualNode = startNode.Neighbors[i].node;

                while (path.Count > 1)
                {
                    if (actualNode != path.Peek() || VisitedNodes.Contains(actualNode) )
                    {
                        //zdjąć ostatni
                        actualNode = path.Pop();
                    }
                    else
                    {
                        VisitedNodes.Add(actualNode);
                        if (actualNode.ConnectedNode != null)//węzeł skojarzony
                        {
                            actualNode = actualNode.ConnectedNode;//przejscie po skojarzeniu
                            path.Push(actualNode);
                        }
                        else
                        {
                            if (actualNode.GroupID != startNode.GroupID)
                            {
                                //mamy ścieżkę rozszerzającą
                                return path.ToArray();//path zawiera ścieżkę rozszerzającą
                            }
                            else
                            {
                                var neighbors = actualNode.Neighbors;
                                int countOfNeighbors = neighbors.Count;
                                bool changedActualNode = false;
                                for (int j = 0; j < countOfNeighbors; j++)
                                {
                                    if (!VisitedNodes.Contains(neighbors[j].node))
                                    {
                                        actualNode = neighbors[j].node;
                                        path.Push(actualNode);
                                        changedActualNode = true;
                                        break;
                                    }
                                }

                                if (!changedActualNode)
                                {
                                    actualNode = path.Pop();//Powrót w przypadku braku innych krawędzi z węzła
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        public static void ChangeDirectionsInTheGraph(Node[] aughmentingPath) 
        {
            if (aughmentingPath == null)
                return;

            int countOfNodes = aughmentingPath.Length;

            for (int index = 0; index < countOfNodes; index++)
            {
                if (aughmentingPath[index].ConnectedNode == null && index+1 != countOfNodes)
                    aughmentingPath[index].ConnectedNode = aughmentingPath[index + 1];
                else
                    aughmentingPath[index].ConnectedNode = null;
            }
            //zamiana skojarzonych krawędzi na wolne
            //jednoczesne wolnych na skojarzone
        }

        private static void PrintResult(Graph graph)
        {
            var firstGroup = graph.GroupsSets[0];
            foreach (var nodeInGroup in firstGroup.NodesInGroup)
            {
                if (nodeInGroup.ConnectedNode != null)
                {
                    if(nodeInGroup.HumanName != null && nodeInGroup.ConnectedNode.HumanName != null)
                        Console.WriteLine($"{nodeInGroup.HumanName} skojarzony z {nodeInGroup.ConnectedNode.HumanName}");
                    else
                        Console.WriteLine($"Węzeł {nodeInGroup.Name} skojarzony z {nodeInGroup.ConnectedNode.Name}");
                }
                else
                {
                    if(nodeInGroup.HumanName != null)
                        Console.WriteLine($"{nodeInGroup.HumanName} nie jest skojarzony.");
                    else
                        Console.WriteLine($"Węzeł {nodeInGroup.Name} nie jest skojarzony.");
                }
                    
                    
            }
        }
    }
}