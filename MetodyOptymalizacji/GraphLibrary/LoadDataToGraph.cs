using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GraphLibrary
{
    public static class GraphLoader
    {
        public static List<Graph> LoadFromFile(string filePath)
        {
            List<Graph> graphs = new List<Graph>();
            using (var file = new StreamReader(filePath))
            {
                while (!file.EndOfStream)
                {
                    var line = file.ReadLine();
                    if (line == "#START_GRAPH")
                        graphs.Add(LoadGraph(file));
                }
            }

            return graphs;
        }

        private static Graph LoadGraph(StreamReader streamReader)
        {
            Graph graph = new Graph();
            int countOfNodesToRead = 0;
            string actualLine = streamReader.ReadLine();
            
            while (actualLine != "#END_GRAPH")
            {
                if (actualLine.StartsWith("#"))
                {
                    var splitedLine = actualLine.Split(' ', ';');
                    switch (splitedLine[0])
                    {
                        case "#PROPERTIES":
                        {
                            if (splitedLine[1] == "1")
                            {
                                graph.IsDirectedGraph = true;
                            }

                            if (splitedLine[2] == "1")
                            {
                                graph.IsWeightedGraph = true;
                            }

                            if (splitedLine[3] == "1")
                            {
                                graph.IsConnectedGraph = true;
                            }

                            if (splitedLine[4] == "1")
                            {
                                graph.IsCompletedGraph = true;
                            }

                            if (splitedLine[5] == "1")
                            {
                                graph.IsBipartitedGraph = true;
                            }
                            
                            break;
                        }

                        case "#VERTICES":
                        {
                            countOfNodesToRead = Int32.Parse(splitedLine[1]);
                            
                            actualLine = streamReader.ReadLine();
                            while (!streamReader.EndOfStream && actualLine != "#END_VERTICES")
                            {
                                var splited = actualLine.Split(' ', ';');
                                if (splited.Length == 2)
                                    graph.AddNodeByName(splited[0], splited[1]);
                                else
                                    graph.AddNodeByName(splited[0]);
                                
                                actualLine = streamReader.ReadLine();
                            }
                            break;
                        }

                        case "#EDGES":
                        {
                            actualLine = streamReader.ReadLine();
                            while (!streamReader.EndOfStream && actualLine != "#END_EDGES")
                            {
                                var splited = actualLine.Split(' ', ';');
                                if (graph.IsWeightedGraph)
                                    graph.AddEdgeByName(splited[0], splited[1], wage: Int32.Parse(splited[2]));
                                else
                                    graph.AddEdgeByName(splited[0], splited[1]);
                                actualLine = streamReader.ReadLine();
                            }
                            break;
                        }
                        case "#CONNECTIONS":
                        {
                            actualLine = streamReader.ReadLine();
                            while (!streamReader.EndOfStream && actualLine != "#END_CONNECTIONS")
                            {
                                var splited = actualLine.Split(' ');
                                graph.AddConnectionByName(splited[0], splited[1]);
                                actualLine = streamReader.ReadLine();
                            }
                            break;
                        }
                        case "#GROUPS":
                        {
                            graph.IsBipartitedGraph = true;
                            actualLine = streamReader.ReadLine();
                            while (!streamReader.EndOfStream && actualLine != "#END_GROUPS")
                            {
                                var splited = actualLine.Split(' ', ';');
                                if (splited[0] != "")
                                {
                                    List<string> nodesInGroupByName = new List<string>(splited);
                                    graph.AddGroup(nodesInGroupByName);
                                }
                                actualLine = streamReader.ReadLine();
                            }
                            break;
                        }
                    }
                }

                actualLine = streamReader.ReadLine();
            }

            if (countOfNodesToRead == graph.CountOfNodes)
            {
                return graph;
            }
            
            return null;
        }

        public static void SaveGraphToFile(string fileName, Graph graph)
        {
            throw new NotImplementedException();
        }
    }
}