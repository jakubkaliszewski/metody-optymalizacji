using System.Collections.Generic;

namespace GraphLibrary
{
    public class Graph
    {
        private List<Node> _nodes;
        private int _groupCounter;
        public List<Group> GroupsSets { get; }
        public int NonMatchedNodes { get; set; }

        public bool IsDirectedGraph { get; set; }
        public bool IsWeightedGraph { get; set; }
        public bool IsConnectedGraph { get; set; }
        public bool IsCompletedGraph { get; set; }
        /*Dwudzielny*/
        public bool IsBipartitedGraph { get; set; }
        public bool IsMatchingGraph { get; set; }

        public int CountOfNodes
        {
             get { return GetAllNodes().Count; }
        }

        private int _countOfDirectedEdges;

        public Graph()
        {
            _nodes = new List<Node>();
            GroupsSets = new List<Group>();
            IsDirectedGraph = false;
            IsWeightedGraph = false;
            IsConnectedGraph = false;
            IsCompletedGraph = false;
            IsBipartitedGraph = false;
        }

        public void AddNodeByName(string nodeName)
        {
            if(!IsNodeExist(nodeName))
                AddNewNode(new Node(nodeName));
        } 
        public void AddNodeByName(string nodeName, string description)
        {
            if(!IsNodeExist(nodeName))
                AddNewNode(new Node(nodeName, description));
        }

        private void AddNewNode(Node newNode)
        {
            _nodes.Add(newNode);
        }

        public List<Node> GetAllNodes()
        {
            return _nodes;
        }

        public void RemoveNode(Node node)
        {
            foreach (var neighbor in node.Neighbors)
            {
                RemoveEdge(neighbor.node, node);
            }

            _nodes.Remove(node);
        }
        
        public void RemoveNodeByName(string name)
        {
            name = name.ToUpper();
            if (_nodes.Exists(node => node.Name == name))
            {
                Node nodeToRemove = _nodes.Find(node => node.Name == name);
                RemoveNode(nodeToRemove);
            }
        }

        public void AddEdgeByName(string firstNodeName, string secondNodeName, bool isDirected = false, int wage = 1)
        {
            bool firstNodeExist = IsNodeExist(firstNodeName);
            bool secondNodeExist = IsNodeExist(secondNodeName);
            Node firstNode, secondNode;

            if (!firstNodeExist)
            {
                firstNode = new Node(firstNodeName);
                AddNewNode(firstNode);
            }
            else
            {
                firstNode = GetNodeByName(firstNodeName);
            }

            if (!secondNodeExist)
            {
                secondNode = new Node(secondNodeName);
                AddNewNode(secondNode);
            }
            else
            {
                secondNode = GetNodeByName(secondNodeName);
            }

            if (IsDirectedGraph)
            {
                _countOfDirectedEdges++;
                AddDirectedEdge(firstNode, secondNode);
            }
            else
                AddEdge(firstNode, secondNode);
        }

        private void AddEdge(Node node1, Node node2, int wage = 1)
        {
            node1.AddNeighbor(node2, wage);
            node2.AddNeighbor(node1, wage);
        }

        private void AddDirectedEdge(Node node1, Node node2, int wage = 1)
        {
            node1.AddNeighbor(node2, wage);
        }

        private void RemoveEdge(Node node1, Node node2)
        {
            if (node1.RemoveNeighbor(node2) && node2.RemoveNeighbor(node1))
                return;

            _countOfDirectedEdges--;
            if (_countOfDirectedEdges == 0)
                IsDirectedGraph = false;
        }

        public void RemoveEdgeByName(string firstNodeName, string secondNodeName)
        {
            var node1 = GetNodeByName(firstNodeName);
            var node2 = GetNodeByName(secondNodeName);

            RemoveEdge(node1, node2);
        }

        private Node GetNodeByName(string name)
        {
            return _nodes.Find(node => node.Name == name.ToUpper());
        }

        private bool IsNodeExist(string nodeName)
        {
            nodeName = nodeName.ToUpper();
            return _nodes.Exists(node => node.Name == nodeName.ToUpper());
        }

        public void AddConnectionByName(string firstNodeName, string secondNodeName)
        {
            bool firstNodeExist = IsNodeExist(firstNodeName);
            bool secondNodeExist = IsNodeExist(secondNodeName);
            Node firstNode, secondNode;

            if (!firstNodeExist)
            {
                firstNode = new Node(firstNodeName);
                AddNewNode(firstNode);
            }
            else
            {
                firstNode = GetNodeByName(firstNodeName);
            }

            if (!secondNodeExist)
            {
                secondNode = new Node(secondNodeName);
                AddNewNode(secondNode);
            }
            else
            {
                secondNode = GetNodeByName(secondNodeName);
            }
            
            AddConnection(firstNode, secondNode);
            IsMatchingGraph = true;
        }

        private void AddConnection(Node node1, Node node2)
        {
            node1.ConnectedNode = node2;
            node2.ConnectedNode = node1;
        }

        public void ChangeEdgeToDirected(Node nodeFrom, Node nodeTo)
        {
            bool nodeFromExist = IsNodeExist(nodeFrom.Name);
            bool nodeToExist = IsNodeExist(nodeTo.Name);

            if (nodeFromExist && nodeToExist) 
            {
                RemoveEdge(nodeFrom, nodeTo);
                AddDirectedEdge(nodeFrom, nodeTo);
            }
        }

        private bool IsEdgeExist(Node node1, Node node2)
        {
            return IsDirectedEdgeExist(node1, node2) && IsDirectedEdgeExist(node2, node1);
        }

        private bool IsDirectedEdgeExist(Node nodeFrom, Node nodeTo)
        {
            return (nodeFrom.IsNeighbor(nodeTo) && !nodeTo.IsNeighbor(nodeFrom));
        }
        
        public void AddGroup(List<string> nodeNames)
        {
            Group newGroup = new Group(_groupCounter);
            _groupCounter++;
            int groupID = newGroup.ID;
            var nodes = new List<Node>();

            foreach (var nodeName in nodeNames)
            {
                Node node;
                if (IsNodeExist(nodeName))
                {
                    node = GetNodeByName(nodeName);
                    node.GroupID = groupID;
                    nodes.Add(node);
                }
                else
                {
                    node = new Node(nodeName) {GroupID = groupID};
                    AddNewNode(node);
                    nodes.Add(node);
                }
            }
            newGroup.AddNodes(nodes);
            AddGroup(newGroup);
        }
        private void AddGroup(Group group)
        {
            GroupsSets.Add(group);
        }
        
        public bool GraphHasPerfectMatching()
        {
            return GroupsSets[0].NodesInGroup.TrueForAll(node => node.ConnectedNode != null);
        }
    }
}