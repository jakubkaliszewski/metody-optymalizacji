﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphLibrary
{
    public class Node
    {
        public List<(Node node, int wage)> Neighbors { get; private set; }
        public Node ConnectedNode { get; set; }
        public int GroupID { get; set; }
        public string Name { get;}
        public string HumanName { get;}
        public int Label { get; set; }
        

        public int CountOfNeighbors 
            => Neighbors.Count;
        
        public Node(string name)
        {
            Neighbors = new List<(Node node, int wage)>();
            Name = name.ToUpper();
            HumanName = Name;
        }
        
        public Node(string name, string humanName)
        {
            Neighbors = new List<(Node node, int wage)>();
            Name = name.ToUpper();
            HumanName = humanName;
        }

        public void AddNeighbor(Node newNeighbor, int wage = 1) 
            => Neighbors.Add((newNeighbor, wage));

        public bool RemoveNeighbor(Node neighbor)
        {
            try
            {
                var toRemove = Neighbors.Find(a => a.node == neighbor);
                Neighbors.Remove(toRemove);
                return true;
            }
            catch (Exception)
            {
                Console.WriteLine("Nie znaleziono takiej krawędzi!");
                return false;
            }
        }

        public bool IsNeighbor(Node node)
        {
            return Neighbors.Exists(a => a.node == node);
        }
    }
}