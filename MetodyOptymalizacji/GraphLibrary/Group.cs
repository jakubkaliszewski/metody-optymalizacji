using System.Collections.Generic;

namespace GraphLibrary
{
    public class Group
    {
        public List<Node> NodesInGroup { get; }
        public int ID { get; }

        public Group(int id)
        {
            NodesInGroup = new List<Node>();
            ID = id;
        }

        public void AddNode(Node node)
        {
            NodesInGroup.Add(node);
        }

        public void AddNodes(List<Node> nodes)
        {
            NodesInGroup.AddRange(nodes);
        }
    }
}