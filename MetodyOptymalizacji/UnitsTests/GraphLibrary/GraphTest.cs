using GraphLibrary;
using Xunit;

namespace UnitsTests.GraphLibrary
{
    public class GraphTest
    {
        [Fact]
        public void CreateGraph()
        {
            var graph = new Graph();
            Assert.NotNull(graph);
        }

        [Fact]
        public void AddFewNodes()
        {
            var graph = new Graph();
            
            graph.AddNodeByName("A");
            graph.AddNodeByName("B");
            graph.AddNodeByName("C");

            Assert.True(graph.GetAllNodes().Count == 3);
        }

        [Fact]
        public void RemoveFewNodes()
        {
            var graph = new Graph();
            
            graph.AddNodeByName("A");
            graph.AddNodeByName("B");
            graph.AddNodeByName("C");

            graph.RemoveNodeByName("B");
            
            Assert.True(graph.GetAllNodes().Count == 2);
        }
        
        [Fact]
        public void AddDoubleSameNode()
        {
            var graph = new Graph();
            
            graph.AddNodeByName("A");
            graph.AddNodeByName("B");
            graph.AddNodeByName("C");
            graph.AddNodeByName("B");
            graph.AddNodeByName("C");
            
            Assert.True(graph.CountOfNodes == 3, "Graf powinien mieć 3 wierzchołki!");
        }
        
    }
}